<?php

use MEDIAESSENZ\MeGoogleCalendar\Controller\CalendarController;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

defined('TYPO3') or die();

ExtensionUtility::configurePlugin(
    'MeGoogleCalendar',
    'Pi1',
    [
        CalendarController::class => 'list',
    ],
    [
    ]
);

ExtensionManagementUtility::addUserTSConfig('
    options.saveDocNew.tx_megooglecalendar_feeds=1
    ');

