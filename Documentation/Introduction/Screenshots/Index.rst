﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: /Includes.rst.txt

Screenshots
^^^^^^^^^^^

.. include:: /Images/google-calendar-week-view.rst.txt

.. include:: /Images/google-calendar-list-view.rst.txt


