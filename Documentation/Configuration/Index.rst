﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.rst.txt

Configuration
-------------


.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   ExtensionManagerSettings/Index
   TypoScriptConstants/Index
   Examples/Index

