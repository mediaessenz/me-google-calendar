﻿.. include:: /Includes.rst.txt

.. highlight:: typoscript

==========
TypoScript
==========

plugin.tx\_megooglecalendar.persistence
---------------------------------------

.. confval:: storagePid

   :type: string
   :Default: Empty
   :Path: plugin.tx\_megooglecalendar.persistence.storagePid

   Storage Pids separated by comma:
   Leave empty to ignore storage page.

.. confval:: recursive

   :type: number
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.persistence.recursive

   Recursive level:
   Only useful if storagePid is set.


plugin.tx\_megooglecalendar.settings
------------------------------------

.. confval:: cssPath

   :type: string
   :Default: EXT:me\_google\_calendar/Resources/Public/Css/main.css
   :Path: plugin.tx\_megooglecalendar.settings.cssPath

   If you want to customize the CSS File, define the path like:
   fileadmin/templates/css/styles.css. Leave empty to not include.

.. confval:: printCssPath

   :type: string
   :Default: EXT:me\_google\_calendar/Resources/Public/Css/fullcalendar.print.css
   :Path: plugin.tx\_megooglecalendar.settings.printCssPath

   Path to print css file.

.. confval:: cssThemePath

   :type: string
   :Default: EXT:me\_google\_calendar/Resources/Public/JavaScript/jquery-ui/themes/smoothness/jquery-ui.min.css
   :Path: plugin.tx\_megooglecalendar.settings.cssThemePath

   Path to CSS theme file. Leave empty to not include.

.. confval:: cssThemePathExternal

   :type: boolean
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.cssThemePathExternal

   Path to cssThemePath is external.

.. confval:: themeSystem

   :type: options
   :Default: standard
   :Path: plugin.tx\_megooglecalendar.settings.themeSystem

   Renders the calendar with a given theme system (standard, bootstrap3, jquery-ui. For more info see https://fullcalendar.io/docs/display/themeSystem/

.. confval:: hideHeader

   :type: boolean
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.hideHeader

   Hide header content.

.. confval:: headerLeft

   :type: string
   :Default: prev,next today
   :Path: plugin.tx\_megooglecalendar.settings.headerLeft

   Left header content. Allowed values: prev, next, today, title, month,
   agendaWeek, agendaDay, basicWeek, basicDay, listDay, listWeek, listMonth,
   listYear, list. Use comma for separation and [Space] for distance.

.. confval:: headerCenter

   :type: string
   :Default: title
   :Path: plugin.tx\_megooglecalendar.settings.headerCenter

   Center header content. Allowed values: prev, next, today, title, month,
   agendaWeek, agendaDay, basicWeek, basicDay, listDay, listWeek, listMonth,
   listYear, list. Use comma for separation and [Space] for distance.

.. confval:: headerRight

   :type: string
   :Default: month,agendaWeek,agendaDay
   :Path: plugin.tx\_megooglecalendar.settings.headerRight

   Right header content. Allowed values: prev, next, today, title, month,
   agendaWeek, agendaDay, basicWeek, basicDay, listDay, listWeek, listMonth,
   listYear, list. Use comma for separation and [Space] for distance.

.. confval:: hideFooter

   :type: boolean
   :Default: 1
   :Path: plugin.tx\_megooglecalendar.settings.hideFooter

   Hide footer content

.. confval:: footerLeft

   :type: string
   :Default: prev,next today
   :Path: plugin.tx\_megooglecalendar.settings.footerLeft

   Left header content. Allowed values: prev, next, today, title, month,
   agendaWeek, agendaDay, basicWeek, basicDay, listDay, listWeek, listMonth,
   listYear, list. Use comma for separation and [Space] for distance.

.. confval:: footerCenter

   :type: string
   :Default: title
   :Path: plugin.tx\_megooglecalendar.settings.footerCenter

   Center footer content. Allowed values: prev, next, today, title, month,
   agendaWeek, agendaDay, basicWeek, basicDay, listDay, listWeek, listMonth,
   listYear, list. Use comma for separation and [Space] for distance.

.. confval:: footerRight

   :type: string
   :Default: month,agendaWeek,agendaDay
   :Path: plugin.tx\_megooglecalendar.settings.footerRight

   Right header content. Allowed values: prev, next, today, title, month,
   agendaWeek, agendaDay, basicWeek, basicDay, listDay, listWeek, listMonth,
   listYear, list. Use comma for separation and [Space] for distance.

.. confval:: defaultView

   :type: string
   :Default: AgendaWeek
   :Path: plugin.tx\_megooglecalendar.settings.defaultView

   Default view of the calendarAllowed values:
   month, agendaWeek, agendaDay, basicWeek, basicDay, listDay, listWeek,
   listMonth, listYear, list

.. confval:: allDaySlot

   :type: boolean
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.allDaySlot

   Show all day events slot in agenda views.

.. confval:: firstDay

   :type: number
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.firstDay

   First day of week. Can be 0 for sunday or 1 for monday.

.. confval:: weekends

   :type: boolean
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.weekends

   Show weekends as well.

.. confval:: weekNumbers

   :type: boolean
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.weekNumbers

   Show week numbers.

.. confval:: weekNumbersWithinDays

   :type: boolean
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.weekNumbersWithinDays

   Show week numbers within days.


.. confval:: weekNumberCalculation

   :type: string
   :Default: ISO
   :Path: plugin.tx\_megooglecalendar.settings.weekNumberCalculation

   Week number calculation.


.. confval:: minTime

   :type: string
   :Default: 00:00:00
   :Path: plugin.tx\_megooglecalendar.settings.minTime

   Mix time for agenda views in format hh:mm:ss


.. confval:: maxTime

   :type: string
   :Default: 24:00:00
   :Path: plugin.tx\_megooglecalendar.settings.maxTime

   Max time for agenda views in format hh:mm:ss

.. confval:: scrollTime

   :type: string
   :Default: 08:00:00
   :Path: plugin.tx\_megooglecalendar.settings.scrollTime

   Scroll to time in format hh:mm:ss

.. confval:: weekMode

   :type: string
   :Default: variable
   :Path: plugin.tx\_megooglecalendar.settings.weekMode

   Defines the way week are shown in month. Can be 'fixed' or 'variable'.

.. confval:: hideTitle

   :type: boolean
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.hideTitle

   Hide title content.

.. confval:: eventLimit

   :type: boolean
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.eventLimit

   Show more events link, if they do not fit into time slot.

.. confval:: titleFormat.month

   :type: string
   :Default: MMMM YYYY
   :Path: plugin.tx\_megooglecalendar.settings.titleFormat.month

   Date format\* for month title.

.. confval:: titleFormat.week

   :type: string
   :Default: MMM D YYYY
   :Path: plugin.tx\_megooglecalendar.settings.titleFormat.week

   Date format\* for week title.

.. confval:: titleFormat.day

   :type: string
   :Default: MMMM D YYYY
   :Path: plugin.tx\_megooglecalendar.settings.titleFormat.day

   Date format\* for day title.

.. confval:: columnFormat.month

   :type: string
   :Default: dddd
   :Path: plugin.tx\_megooglecalendar.settings.columnFormat.month

   Defines the column format\* for the month view.

.. confval:: columnFormat.week

   :type: string
   :Default: ddd DD.MM.
   :Path: plugin.tx\_megooglecalendar.settings.columnFormat.week

   Defines the column format\* for the week view.

.. confval:: columnFormat.day

   :type: string
   :Default: dddd DD.MM.
   :Path: plugin.tx\_megooglecalendar.settings.columnFormat.day

   Defines the column format\* for the day view.

.. confval:: listDayFormat

   :type: string
   :Default: DD. MMMM YYYY
   :Path: plugin.tx\_megooglecalendar.settings.listDayFormat

   Defines the left date format\* for the list view.

.. confval:: listDayAltFormat

   :type: string
   :Default: ddddd
   :Path: plugin.tx\_megooglecalendar.settings.listDayAltFormat

   Defines the right date format\* for the list view.

.. confval:: timeFormat.agenda

   :type: string
   :Default: HH:mm
   :Path: plugin.tx\_megooglecalendar.settings.timeFormat.agenda

   Time format\* for agenda view.

.. confval:: timeFormat.list

   :type: string
   :Default: HH:mm
   :Path: plugin.tx\_megooglecalendar.settings.timeFormat.list

   Time format\* for list view.

.. confval:: timeFormat.day

   :type: string
   :Default: HH:mm
   :Path: plugin.tx\_megooglecalendar.settings.timeFormat.day

   Time format\* for day view.

.. confval:: timeFormat.week

   :type: string
   :Default: HH:mm
   :Path: plugin.tx\_megooglecalendar.settings.timeFormat.week

   Time format\* for week view.

.. confval:: timeFormat.month

   :type: string
   :Default: HH:mm
   :Path: plugin.tx\_megooglecalendar.settings.timeFormat.month

   Time format\* for month view.

.. confval:: timeFormat.general

   :type: string
   :Default: HH:mm
   :Path: plugin.tx\_megooglecalendar.settings.timeFormat.general

   General time format\*.

.. confval:: timeZone

   :type: string
   :Default:
   :Path: plugin.tx\_megooglecalendar.settings.timeZone

   e.g. Europe/Berlin. If empty (default) the timezone defined inside google UI will be used.

.. confval:: noGoogleMapsLink

   :type: boolean
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.noGoogleMapsLink

   Do not generate link to google maps.

.. confval:: hideIcalDownloadButton

   :type: boolean
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.hideIcalDownloadButton

   Hide ical download button in event details.

.. confval:: hideAddtoGoogleCalendarButton

   :type: boolean
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.hideAddtoGoogleCalendarButton

   Hide add to google calendar button.

.. confval:: height

   :type: integer
   :Default: 0
   :Path: plugin.tx\_megooglecalendar.settings.height

   Height of calendar.

.. confval:: aspectRatio

   :type: string
   :Default: 1.35
   :Path: plugin.tx\_megooglecalendar.settings.aspectRatio

   Aspect ratio.

.. confval:: overrideFlexformSettingsIfEmpty

   :type: string
   :Default: cssThemePath, headerLeft, headerCenter, headerRight, footerLeft, footerCenter, footerRight, defaultView,
                      allDaySlot, firstDay, firstHour, weekends, minTime, maxTime, hideTitle, hideIcalDownloadButton,
                      hideAddtoGoogleCalendarButton, noGoogleMapsLink, height
   :Path: plugin.tx\_megooglecalendar.settings.overrideFlexformSettingsIfEmpty

   Override flexform setting if empty.

.. confval:: eIdGetIcsUrl

   :type: string
   :Default: /index.php?eID=meGoogleCalendarEidGetIcs
   :Path: plugin.tx\_megooglecalendar.settings.eIdGetIcsUrl

   eID Script URL for ics file generation.

.. confval:: language

   :type: string
   :Default: de
   :Path: plugin.tx\_megooglecalendar.settings.language

   Language key.


\* For more infos about the time/date format, please read the
documentation on the website of the FullCalendar plugin author:

`https://fullcalendar.io/docs/date-library#formatdates
<https://fullcalendar.io/docs/date-library#formatdates>`_
