﻿.. include:: /Includes.rst.txt

=======================
Extension Configuration
=======================


.. confval:: restrictToPredefinedCssClasses

   :type: boolean
   :Default: 0

   Restrict google calendar record css to predefined styles. See manual for more info
