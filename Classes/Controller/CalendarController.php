<?php
namespace MEDIAESSENZ\MeGoogleCalendar\Controller;

use MEDIAESSENZ\MeGoogleCalendar\Domain\Repository\CalendarRepository;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Alexander Grein <alexander.grein@gmail.com>, MEDIA::ESSENZ
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package me_google_calendar
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class CalendarController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var CalendarRepository
     */
    protected $calendarRepository;

    public function __construct(CalendarRepository $calendarRepository) {
        $this->calendarRepository = $calendarRepository;
    }

    public function initializeAction(): void
    {
        $mergedSettings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);

        $fullTyposcriptSettings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        $tsSetupSettings = $fullTyposcriptSettings['plugin.']['tx_megooglecalendar.']['settings.'];

        // start override
        if (isset($tsSetupSettings['overrideFlexformSettingsIfEmpty'])) {
            $overrideIfEmpty = GeneralUtility::trimExplode(',',
                $tsSetupSettings['overrideFlexformSettingsIfEmpty'], true);
            foreach ($overrideIfEmpty as $key) {
                // if flexform setting is empty and value is available in TS
                if ((empty($mergedSettings[$key]))
                    && isset($tsSetupSettings[$key])
                ) {
                    $mergedSettings[$key] = $tsSetupSettings[$key];
                }
            }
        }

        $this->settings = $mergedSettings;
    }

    /**
     * action list
     *
     * @return ResponseInterface
     * @throws InvalidQueryException
     */
    public function listAction(): ResponseInterface
    {
        $calendarRecordUidsCsv = $this->settings['calendarRecords'];
        $calendarRecordUidsArray = GeneralUtility::trimExplode(',', $calendarRecordUidsCsv, true);
        if (count($calendarRecordUidsArray) > 0) {
            $calendars = $this->calendarRepository->findByUids($calendarRecordUidsArray);
        } else {
            $calendars = $this->calendarRepository->findAllCalendarRecords();
        }

        $this->view->assign('calendars', $calendars);
        $versionInformation = GeneralUtility::makeInstance(Typo3Version::class);
        if ($versionInformation->getMajorVersion() <= 12) {
            $this->view->assign('data', $this->configurationManager->getContentObject()->data);
        }
        return $this->htmlResponse();
    }
}
