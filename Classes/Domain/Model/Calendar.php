<?php

namespace MEDIAESSENZ\MeGoogleCalendar\Domain\Model;

use TYPO3\CMS\Extbase\Annotation as Extbase;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUserGroup;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Alexander Grein <alexander.grein@gmail.com>, MEDIA::ESSENZ
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package me_google_calendar
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Calendar extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

  /**
   * title
   *
   * @var string
   * @Extbase\Validate("NotEmpty")
   */
  protected string $title;

  /**
   * @var string
   * @Extbase\Validate("NotEmpty")
   */
  protected string $googleCalendarApiKey;

  /**
   * @var string
   * @Extbase\Validate("NotEmpty")
   */
  protected string $googleCalendarId;

  /**
   * css
   *
   * @var string
   */
  protected string $css;

  /**
   * fe_groups
   *
   * @var ObjectStorage<FrontendUserGroup>
   */
  protected $feGroups;

  /**
   * Returns the title
   *
   * @return string $title
   */
  public function getTitle(): string
  {
    return $this->title;
  }

  /**
   * Sets the title
   *
   * @param string $title
   *
   * @return void
   */
  public function setTitle(string $title): void
  {
    $this->title = $title;
  }

  /**
   * @return string
   */
  public function getGoogleCalendarApiKey(): string
  {
    return $this->googleCalendarApiKey;
  }

  /**
   * @param string $googleCalendarApiKey
   */
  public function setGoogleCalendarApiKey(string $googleCalendarApiKey): void
  {
    $this->googleCalendarApiKey = $googleCalendarApiKey;
  }

  /**
   * @return string
   */
  public function getGoogleCalendarId(): string
  {
    return $this->googleCalendarId;
  }

  /**
   * @param string $googleCalendarId
   */
  public function setGoogleCalendarId(string $googleCalendarId): void
  {
    $this->googleCalendarId = $googleCalendarId;
  }

  /**
   * Returns the css
   *
   * @return string $css
   */
  public function getCss(): string
  {
    return $this->css;
  }

  /**
   * Sets the css
   *
   * @param string $css
   *
   * @return void
   */
  public function setCss(string $css): void
  {
    $this->css = $css;
  }

  /**
   * @param ObjectStorage $feGroups
   */
  public function setFeGroups(ObjectStorage $feGroups): void
  {
    $this->feGroups = $feGroups;
  }

  /**
   * @return ObjectStorage
   */
  public function getFeGroups(): ObjectStorage
  {
    return $this->feGroups;
  }

}
