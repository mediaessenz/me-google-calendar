<?php
declare(strict_types=1);

namespace MEDIAESSENZ\MeGoogleCalendar\Middleware;

use DateTimeImmutable;
use DateTimeZone;
use Exception;
use JsonException;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Charset\CharsetConverter;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class CalendarIcsMiddleware implements MiddlewareInterface
{
    public function __construct(private ResponseFactoryInterface $responseFactory)
    {}

    /**
     * @throws JsonException
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (!$this->isMatchingRequest($request)) {
            return $handler->handle($request);
        }

        $parameters = $request->getQueryParams();

        $requiredParameters = ['uid', 'dtstart', 'dtend'];
        if (count(array_intersect_key(array_flip($requiredParameters), $parameters)) !== count($requiredParameters)) {
            return $this->responseFactory->createResponse()->withStatus(500);
        }

        $allDay = !str_contains($parameters['dtstart'], 'T');

        $timeZoneString = $parameters['timezone'] ?: date_default_timezone_get();
        $filenameTimeZone = new DateTimeZone($timeZoneString);
        $utcTimeZone = new DateTimeZone('UTC');

        $dtstartDateTime = new DateTimeImmutable($parameters['dtstart'], $utcTimeZone);
        $filenameStartDateTime = clone $dtstartDateTime;
        $filenameStartDateTime = $filenameStartDateTime->setTimezone($filenameTimeZone);
        $dtstartDate = $dtstartDateTime->format('Ymd');
        $dtstartTime = $dtstartDateTime->format('His');
        $filenameStartTime = $allDay ? '' : '_' . $filenameStartDateTime->format('Hi');

        $dtendDateTime = new DateTimeImmutable($parameters['dtend'], $utcTimeZone);
        $filenameEndDateTime = clone $dtendDateTime;
        $filenameEndDateTime = $filenameEndDateTime->setTimezone($filenameTimeZone);
        $dtendDate = $dtendDateTime->format('Ymd');
        $dtendTime = $dtendDateTime->format('His');
        $filenameEndTime = $allDay ? '' : '_' . $filenameEndDateTime->format('Hi');

        if ($allDay && $filenameEndDateTime->diff($filenameStartDateTime)->days > 1) {
            $filenameEndDateTime = $filenameEndDateTime->modify('-1 day');
            $filenameEndTime .= $filenameEndDateTime->format('Ymd') . $filenameEndTime;
        }

        if ($filenameEndTime) {
            $filenameEndTime = '-' . $filenameEndTime;
        }

        $filenameEndTime = str_replace('-_', '-', $filenameEndTime);

        $fileName = ($parameters['summary'] ?? 'event') . '_' . $dtstartDate . $filenameStartTime . $filenameEndTime . '.ics';

        $fileName = GeneralUtility::makeInstance(CharsetConverter::class)->utf8_char_mapping($fileName);
        // Replace unwanted characters by underscores
        $cleanFileName = rtrim(preg_replace('/[\\x00-\\x2C\\/\\x3A-\\x3F\\x5B-\\x60\\x7B-\\xBF\\xC0-\\xFF]/', '_', trim($fileName)) ?? '', '.');

        $content = 'BEGIN:VCALENDAR
VERSION:2.0
PRODID:TYPO3//MeGoogleCalendar
CALSCALE:GREGORIAN
METHOD:PUBLISH
BEGIN:VEVENT
UID:' . htmlspecialchars($parameters['uid']) . '
SUMMARY:' . htmlspecialchars($parameters['summary'] ?? '') . '
DTSTAMP:' . date('Ymd\THis\Z') . '
DTSTART:' . $dtstartDate . ($allDay ? '' : 'T' . $dtstartTime . 'Z') . '
DTEND:' . $dtendDate . ($allDay ? '' : 'T' . $dtendTime . 'Z') . '
' . (($parameters['description'] ?? 'undefined') !== 'undefined' ? 'DESCRIPTION:' . self::secureString($parameters['description'] ?? '') : '') . '
' . ($parameters['location'] ?? false ? 'LOCATION:' . self::secureString($parameters['location'] ?? '') : '') . '
END:VEVENT
END:VCALENDAR';

        $response = $this->responseFactory->createResponse()->withHeader('Content-Type', 'text/calendar; charset=utf-8');
        $response = $response->withAddedHeader('Content-Disposition', 'attachment; filename=' . urlencode($cleanFileName));
        $response = $response->withAddedHeader('Pragma', 'no-cache');
        $response = $response->withAddedHeader('Expire', '0');
        $response->getBody()->write($content);

        return $response;
    }

    protected function isMatchingRequest(ServerRequestInterface $request): bool
    {
        return $request->getUri()->getPath() === '/calendar.ics';
    }

    private static function secureString($string): string
    {
        return trim(str_replace(['\\', "\r", "\n", ',', ';'], ['\\\\', '', '\n', '\,', '\;'], html_entity_decode(strip_tags(str_replace('<br>', '\\n', $string)), ENT_COMPAT, 'UTF-8')));
    }

}
