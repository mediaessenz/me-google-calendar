<?php

return [
    'frontend' => [
        'mediaessenz/me-google-calendar/calendar-ics' => [
            'target' => \MEDIAESSENZ\MeGoogleCalendar\Middleware\CalendarIcsMiddleware::class,
            'before' => [
                'typo3/cms-frontend/site',
            ],
        ],
    ],
];
