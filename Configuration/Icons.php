<?php

use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;

return [
    'tx-megooglecalendar-calendar' => [
        'provider' => SvgIconProvider::class,
        'source' => 'EXT:me_google_calendar/Resources/Public/Icons/tx_megooglecalendar_domain_model_calendar.svg'
    ],
];
