TYPO3 Extension "me_google_calendar"
====================================

Make use of the jQuery Plugin FullCalendar (v3.10.2) to generates a skin-able calendar with different views
(month, week, day, week list, day list etc.) from one or more Google Calendar XML Feed(s).
